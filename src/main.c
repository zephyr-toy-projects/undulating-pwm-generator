/**
 * Volcano lighting program to have undulating lighting
 */

#include <string.h>
#include <zephyr/device.h>
#include <zephyr/kernel.h>
#include <zephyr/drivers/adc.h>
#include <zephyr/drivers/pwm.h>
#include <zephyr/drivers/uart.h>
#include <zephyr/logging/log.h>
#include <zephyr/sys/printk.h>
#include <zephyr/sys/util.h>
#include <zephyr/usb/usb_device.h>

#include "brightness.h"
#include "cycle.h"

/* Check overlay exists for CDC UART console */
BUILD_ASSERT(DT_NODE_HAS_COMPAT(DT_CHOSEN(zephyr_console), zephyr_cdc_acm_uart),
             "Console device is not ACM CDC UART device");

// CHeck that the ADC has been configured
BUILD_ASSERT(DT_NODE_EXISTS(DT_PATH(zephyr_user)),
             "ADC zephyr,user has not been configured");
BUILD_ASSERT(DT_NODE_HAS_PROP(DT_PATH(zephyr_user), io_channels),
             "ADC user io-channels has not been configured");

LOG_MODULE_REGISTER(main);

#define PWM_PERIOD PWM_HZ(2000)
#define MIN_PERIOD (PWM_PERIOD / 2U)
#define RANGE_PERIOD (PWM_PERIOD * 3 / 10U)
#define MIN_PERIOD_DELTA (PWM_PERIOD * 2 / 10U)
#define MIN_CYCLE_S 2
#define MAX_CYCLE_S 5
#define CYCLE_RANGE_S 3
#define LEVELS_READING_MS 500U

#define DT_SPEC_AND_COMMA(node_id, prop, idx) \
  ADC_DT_SPEC_GET_BY_IDX(node_id, idx),

/* Data of ADC io-channels specified in devicetree. */
static const struct adc_dt_spec adc_channels[] = {
    DT_FOREACH_PROP_ELEM(DT_PATH(zephyr_user),
                         io_channels,
                         DT_SPEC_AND_COMMA)};

static const struct pwm_dt_spec pwm0 =
    PWM_DT_SPEC_GET(DT_CHILD(DT_PATH(pwms), pwm0));
static const struct pwm_dt_spec pwm1 =
    PWM_DT_SPEC_GET(DT_CHILD(DT_PATH(pwms), pwm1));
static const struct pwm_dt_spec pwm2 =
    PWM_DT_SPEC_GET(DT_CHILD(DT_PATH(pwms), pwm2));
static const struct pwm_dt_spec pwm3 =
    PWM_DT_SPEC_GET(DT_CHILD(DT_PATH(pwms), pwm3));
static const struct pwm_dt_spec pwm4 =
    PWM_DT_SPEC_GET(DT_CHILD(DT_PATH(pwms), pwm4));
static const struct pwm_dt_spec pwm5 =
    PWM_DT_SPEC_GET(DT_CHILD(DT_PATH(pwms), pwm5));
static const struct pwm_dt_spec pwm6 =
    PWM_DT_SPEC_GET(DT_CHILD(DT_PATH(pwms), pwm6));
static const struct pwm_dt_spec pwm7 =
    PWM_DT_SPEC_GET(DT_CHILD(DT_PATH(pwms), pwm7));

struct pwm_state
{
  const struct pwm_dt_spec *spec;
  struct cycle cycle;
};

int initialize_usb_serial(bool wait_for_uart_line_ctrl)
{
  /* Configure to set Console output to USB Serial */
  const struct device *usb_device = DEVICE_DT_GET(DT_CHOSEN(zephyr_console));
  uint32_t dtr = 0;

  if (usb_enable(NULL) != 0)
  {
    LOG_ERR("Could not enable USB device");
    return 1;
  }

  if (!wait_for_uart_line_ctrl)
  {
    return 0;
  }

  // Wait for a console connection, if the DTR flag was set to activate USB.
  while (!dtr)
  {
    uart_line_ctrl_get(usb_device, UART_LINE_CTRL_DTR, &dtr);
    k_sleep(K_MSEC(100));
  }

  return 0;
}

int initialize_adc()
{
  /* Configure channels individually prior to sampling. */
  for (size_t i = 0U; i < ARRAY_SIZE(adc_channels); i++)
  {
    if (!adc_is_ready_dt(&adc_channels[i]))
    {
      LOG_ERR("ADC controller device %s not ready\n", adc_channels[i].dev->name);
      return 1;
    }

    int err = adc_channel_setup_dt(&adc_channels[i]);
    if (err < 0)
    {
      LOG_ERR("Could not setup channel #%d (%d)\n", i, err);
      return 1;
    }
  }

  return 0;
}

// Check that all PWMs are ready.  Return nonzero if not.
int pwms_ready(const struct pwm_state pwms[], const int pwm_size)
{
  for (int i = 0; i < pwm_size; ++i)
  {
    if (!pwm_is_ready_dt(pwms[i].spec))
    {
      LOG_ERR("PWM device %s channel %d is not ready",
              pwms[i].spec->dev->name,
              pwms[i].spec->channel);
      return 0;
    }
  }

  return 1;
}

void log_cycle(const struct pwm_state *pwm)
{
  const struct cycle *c = &pwm->cycle;

  switch (c->state)
  {
  case REMAIN:
    LOG_INF("PWM ch %d REMAIN cycle: current: %d, steps: %d",
            pwm->spec->channel,
            c->pulse,
            c->remaining_steps);
    break;
  case INCREMENT:
    LOG_INF("PWM ch %d INCREMENT cycle: current: %d, target: %d steps: %d",
            pwm->spec->channel,
            c->pulse,
            c->target_pulse,
            c->remaining_steps);
    break;
  case DECREMENT:
    LOG_INF("PWM ch %d DECREMENT cycle: current: %d, target: %d steps: %d",
            pwm->spec->channel,
            c->pulse,
            c->target_pulse,
            c->remaining_steps);
    break;
  }

  return;
}

int main(void)
{
  if (initialize_usb_serial(true))
  {
    LOG_ERR("Could not set console output to USB serial");
    return 0;
  }

  LOG_INF("PWM Pulsing Brightness Generator begin main");

  // create the PWM state data structures
  struct pwm_state pwms[] = {
      {&pwm0, {}},
      {&pwm1, {}},
      {&pwm2, {}},
      {&pwm3, {}},
      {&pwm4, {}},
      {&pwm5, {}},
      {&pwm6, {}},
      {&pwm7, {}}};

  if (initialize_adc())
  {
    LOG_ERR("Cannot start main loop without ADC...exiting");
    return 0;
  }

  // initialize brightness levels
  const struct brightness_params bparams = {
      .low = &adc_channels[0],
      .high = &adc_channels[1],
      .delta = &adc_channels[2],
      .pwm_period = PWM_PERIOD};
  struct brightness_levels blevels;

  if (get_brightness_levels(&bparams, &blevels))
  {
    LOG_ERR("Could not initialize brightness levels...exiting");
    return 0;
  }

  for (int i = 0; i < ARRAY_SIZE(pwms); ++i)
  {
    initialize_cycle(blevels.max, &pwms[i].cycle);
    log_cycle(&pwms[i]);
  }

  if (!pwms_ready(pwms, ARRAY_SIZE(pwms)))
  {
    LOG_ERR("Cannot start main loop without PWM...exiting");
    return 0;
  }

  // initialize the PWM channels
  int ret;
  for (int i = 0; i < ARRAY_SIZE(pwms); ++i)
  {
    ret = pwm_set_dt(pwms[i].spec, PWM_PERIOD, pwms[i].cycle.pulse);
    if (ret)
    {
      LOG_ERR("Unable to initialize PWM device %s channel %d, %d",
              pwms[i].spec->dev->name,
              pwms[i].spec->channel,
              ret);
      return 0;
    }
  }

  const uint32_t steps_per_reading = LEVELS_READING_MS / cycle_step_time_ms();

  // Begin main loop using a for loop with count for periodic ADC readings.
  // The overflow of the count should not be a problem, worst case resulting
  // in an extra ADC reading if this loops for years.
  for (uint32_t c = 1; true; ++c)
  {
    // little hack: right side of '&&' will only execute when left side is true
    if (c % steps_per_reading == 0 &&
        get_brightness_levels(&bparams, &blevels))
    {
      LOG_WRN("Could not update brightness levels from ADC: levels may behave unexpectedly.");
    }

    for (int i = 0; i < ARRAY_SIZE(pwms); ++i)
    {
      next_cycle_step(&blevels, &pwms[i].cycle);

      ret = pwm_set_pulse_dt(pwms[i].spec, pwms[i].cycle.pulse);
      if (ret)
      {
        LOG_ERR("Failed to set pulse width with error: %d", ret);
        return 0;
      }
    }

    k_msleep(cycle_step_time_ms());
  }

  return 0;
}
