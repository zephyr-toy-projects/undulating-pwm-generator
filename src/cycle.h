#ifndef SRC_CYCLE_H_
#define SRC_CYCLE_H_

#include <stdint.h>

struct brightness_levels;

enum cycle_state
{
  REMAIN,
  INCREMENT,
  DECREMENT
};

struct cycle
{
  enum cycle_state state;
  uint32_t pulse;
  uint32_t target_pulse;
  uint32_t remaining_steps;
  uint32_t remain_count;
};

uint32_t cycle_step_time_ms();

/*
 * Initialized cycle to the given pulse such that the next call to
 * next_step will refresh the cycle.
 */
void initialize_cycle(const uint32_t pulse, struct cycle *cycle);

/*
 * Advances the cycle to the next step or refreshes the cycle.
 */
void next_cycle_step(const struct brightness_levels *levels, struct cycle *cycle);

#endif