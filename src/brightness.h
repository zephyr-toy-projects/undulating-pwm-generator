#ifndef SRC_BRIGHTNESS_H_
#define SRC_BRIGHTNESS_H_

#include <zephyr/drivers/adc.h>

struct brightness_params
{
  const struct adc_dt_spec *low;
  const struct adc_dt_spec *high;
  const struct adc_dt_spec *delta;
  const uint32_t pwm_period;
};

struct brightness_levels
{
  uint32_t min;
  uint32_t max;
  uint32_t max_delta;
};

int get_brightness_levels(
    const struct brightness_params *params,
    struct brightness_levels *levels);

#endif