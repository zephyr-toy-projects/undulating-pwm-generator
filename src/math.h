#ifndef SRC_MATH_H_
#define SRC_MATH_H_

#include <stdint.h>

static int compare_rationals(const uint32_t num_a,
                             const uint32_t den_a,
                             const uint32_t num_b,
                             const uint32_t den_b)
{
  const uint32_t a = num_a * den_b;
  const uint32_t b = den_a * num_b;

  if (a == b)
  {
    return 0;
  }

  if (a < b)
  {
    return -1;
  }

  return 1;
}

#endif
