#include "brightness.h"
#include "cycle.h"
#include "math.h"
#include <zephyr/logging/log.h>
#include <zephyr/random/random.h>

LOG_MODULE_REGISTER(cycle);

#define CYCLE_STEP_MS 100U
#define CYCLE_MIN_MS 1000U
#define CYCLE_MAX_MS 4000U
#define CYCLE_MS_DELTA (CYCLE_MAX_MS - CYCLE_MIN_MS)

static void refresh_cycle(const struct brightness_levels *levels, struct cycle *cycle);
static void update_cycle_levels(const struct brightness_levels *levels, struct cycle *cycle);

inline uint32_t cycle_step_time_ms()
{
  return CYCLE_STEP_MS;
}

void initialize_cycle(const uint32_t pulse, struct cycle *cycle)
{
  cycle->state = REMAIN;
  cycle->pulse = pulse;
  cycle->target_pulse = pulse;
  cycle->remaining_steps = 0;
  cycle->remain_count = 0;
}

void next_cycle_step(const struct brightness_levels *levels, struct cycle *cycle)
{
  if (cycle->remaining_steps == 0)
  {
    refresh_cycle(levels, cycle);
    return;
  }

  update_cycle_levels(levels, cycle);

  const uint32_t div = cycle->remaining_steps--;

  switch (cycle->state)
  {
  case REMAIN:
    break;
  case INCREMENT:
    if (cycle->target_pulse < cycle->pulse)
    {
      LOG_WRN("increment target pulse less than current pulse");
      cycle->pulse = cycle->target_pulse;
    }

    cycle->pulse += (cycle->target_pulse - cycle->pulse) / div;
    break;
  case DECREMENT:
    if (cycle->target_pulse > cycle->pulse)
    {
      LOG_WRN("decrement target pulse greater than current pulse");
      cycle->pulse = cycle->target_pulse;
    }

    cycle->pulse -= (cycle->pulse - cycle->target_pulse) / div;
    break;
  }
}

static void refresh_cycle(const struct brightness_levels *levels, struct cycle *cycle)
{
  cycle->remaining_steps = (CYCLE_MIN_MS + sys_rand32_get() % CYCLE_MS_DELTA) / CYCLE_STEP_MS;

  // first refresh of cycle will have 50% remain chance
  const uint32_t dist = 2 + cycle->remain_count;
  if (sys_rand32_get() % dist == 0) // REMAIN
  {
    cycle->state = REMAIN;
    cycle->remain_count++;
    return;
  }

  const uint32_t delta = sys_rand32_get() % levels->max_delta;

  // if pulse is almost the max or we roll decrement
  if (compare_rationals(cycle->pulse, levels->max, 19, 20) < 0 || sys_rand32_get() % 2 == 0)
  {
    // if pulse is almost the min
    if (compare_rationals(levels->min, cycle->pulse, 19, 20) < 0)
    {
      cycle->state = INCREMENT;
      cycle->target_pulse = MIN(cycle->pulse + delta, levels->max);
      return;
    }

    cycle->state = DECREMENT;
    cycle->target_pulse = delta > cycle->pulse
                              ? levels->min
                              : MAX(cycle->pulse - delta, levels->min);
    return;
  }

  // default to increment
  cycle->state = INCREMENT;
  cycle->target_pulse = MIN(cycle->pulse + delta, levels->max);
}

static void update_cycle_levels(const struct brightness_levels *levels, struct cycle *cycle)
{
  if (cycle->pulse < levels->min)
  {
    cycle->pulse = levels->min;
  }

  if (cycle->target_pulse < levels->min)
  {
    cycle->target_pulse = levels->min;
  }

  if (cycle->pulse > levels->max)
  {
    cycle->pulse = levels->max;
  }

  if (cycle->target_pulse > levels->max)
  {
    cycle->target_pulse = levels->max;
  }
}
