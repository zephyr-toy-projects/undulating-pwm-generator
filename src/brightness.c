#include "brightness.h"
#include "math.h"
#include <zephyr/drivers/adc.h>
#include <zephyr/logging/log.h>

LOG_MODULE_REGISTER(brightness);

#define BRIGHT_SCALE 100U
#define BRIGHT_LOW_MIN 10U
#define BRIGHT_MID 50U
#define BRIGHT_HIGH_MAX 100U
#define BRIGHT_LOW_TOLERANCE 1U
#define BRIGHT_HIGH_TOLERANCE 99U
#define BRIGHT_MIN_DELTA 20U
#define BRIGHT_MAX_DELTA (BRIGHT_HIGH_MAX - BRIGHT_LOW_MIN)

/*
 * Returns the minimum brightness pulse for a reading with scale and period.
 */
static int get_brightness_min(const uint32_t reading,
                              const uint32_t scale,
                              const uint32_t period)
{
  const uint32_t pulse_min = (period * BRIGHT_LOW_MIN) / BRIGHT_SCALE;

  if (compare_rationals(reading, scale, BRIGHT_LOW_TOLERANCE, BRIGHT_SCALE) < 0)
  {
    return pulse_min;
  }

  const uint32_t pulse_max = (period * BRIGHT_MID) / BRIGHT_SCALE;

  if (compare_rationals(reading, scale, BRIGHT_HIGH_TOLERANCE, BRIGHT_SCALE) > 0)
  {
    return pulse_max;
  }

  return pulse_min + (((pulse_max - pulse_min) * reading) / scale);
}

/*
 * Returns the maximum brightness pulse for a reading with scale and period.
 */
static int get_brightness_max(const uint32_t reading,
                              const uint32_t scale,
                              const uint32_t period)
{
  const uint32_t pulse_max = (period * BRIGHT_HIGH_MAX) / BRIGHT_SCALE;

  if (compare_rationals(reading, scale, BRIGHT_HIGH_TOLERANCE, BRIGHT_SCALE) > 0)
  {
    return pulse_max;
  }

  const uint32_t pulse_min = (period * BRIGHT_MID) / BRIGHT_SCALE;

  if (compare_rationals(reading, scale, BRIGHT_LOW_TOLERANCE, BRIGHT_SCALE) < 0)
  {
    return pulse_min;
  }

  return pulse_min + (((pulse_max - pulse_min) * reading) / scale);
}

/*
 * Returns the maximum brightness pulse for a reading with scale and period.
 */
static int get_brightness_delta(const uint32_t reading,
                                const uint32_t scale,
                                const uint32_t max_delta)
{
  const uint32_t min_delta = (max_delta * BRIGHT_MIN_DELTA) / BRIGHT_SCALE;

  if (compare_rationals(reading, scale, BRIGHT_LOW_TOLERANCE, BRIGHT_SCALE) < 0)
  {
    return min_delta;
  }

  if (compare_rationals(reading, scale, BRIGHT_HIGH_TOLERANCE, BRIGHT_SCALE) > 0)
  {
    return max_delta;
  }

  return min_delta + (((max_delta - min_delta) * reading) / scale);
}

int get_brightness_levels(const struct brightness_params *params, struct brightness_levels *levels)
{
  uint16_t buf;
  struct adc_sequence sequence = {
      .buffer = &buf,
      .buffer_size = sizeof(buf),
  };

  if (adc_sequence_init_dt(params->low, &sequence) ||
      adc_read_dt(params->low, &sequence))
  {
    LOG_ERR("Could not read low brightness level");
    return 1;
  }

  levels->min = get_brightness_min(buf, 1 << params->low->resolution, params->pwm_period);

  if (adc_sequence_init_dt(params->high, &sequence) ||
      adc_read_dt(params->high, &sequence))
  {
    LOG_ERR("Could not read high brightness level");
    return 1;
  }

  levels->max = get_brightness_max(buf, 1 << params->high->resolution, params->pwm_period);

  if (adc_sequence_init_dt(params->delta, &sequence) ||
      adc_read_dt(params->delta, &sequence))
  {
    LOG_ERR("Could not read brightness delta level");
    return 1;
  }

  levels->max_delta = get_brightness_delta(buf, 1 << params->delta->resolution, levels->max - levels->min);

  LOG_DBG("Calculated levels: min: %d max: %d delta: %d",
          levels->min,
          levels->max,
          levels->max_delta);

  return 0;
}